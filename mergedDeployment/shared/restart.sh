#!/usr/bin/env sh

# Bring it down
docker-compose kill
docker-compose down -v
docker rm $(docker ps -aq)
docker rmi $(docker images -aq)


export PROFILES='production' # strip any leading commas
export spring_profiles_active=$PROFILES
echo "Profiles to use: $spring_profiles_active"

# start it up
docker-compose up --build --force-recreate -d
