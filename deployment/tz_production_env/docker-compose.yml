version: '3.5'
services:

  reverse-proxy:
    restart: always
    image: 'traefik:v2.9'
    container_name: 'reverse-proxy'
    command:
      - '--api.insecure=true'
      - '--entrypoints.web.address=:80'
      - '--entrypoints.web.http.redirections.entryPoint.to=websecure'
      - '--entrypoints.web.http.redirections.entryPoint.scheme=https'
      - '--entrypoints.web.http.redirections.entrypoint.permanent=true'
      - '--providers.docker=true'
      - '--providers.docker.exposedbydefault=false'
      - '--entrypoints.websecure.address=:443'
      - '--entryPoints.websecure.forwardedHeaders.insecure=true'
      - '--entryPoints.websecure.proxyProtocol.insecure=true'
      - '--certificatesresolvers.myresolver.acme.tlschallenge=true'
      - '--certificatesresolvers.myresolver.acme.email=info@elmis-dev.org'
      - '--certificatesresolvers.myresolver.acme.storage=/letsencrypt/acme.json'

    ports:
      - '80:80'
      - '443:443'
    volumes:
      - '/var/lib/docker/volumes/letsencrypt:/letsencrypt'
      - '/var/run/docker.sock:/var/run/docker.sock:ro'

  consul:
    command: -server -bootstrap
    image: gliderlabs/consul-server
    ports:
      - "8300"
      - "8400"
      - "8500:8500"
      - "53"

  nginx:
    image: openlmis/nginx:${OL_NGINX_VERSION}
    env_file: settings.env
    environment:
      NGINX_LOG_DIR: '/var/log/nginx/log'
    volumes:
      - 'nginx-log:/var/log/nginx/log'
      - 'consul-template-log:/var/log/consul-template'
    labels:
      - 'traefik.enable=true'
      - 'traefik.http.routers.nginx.rule=Host(`elmis.moh.go.tz`)'
      - 'traefik.http.routers.nginx.entrypoints=websecure'
      - 'traefik.http.routers.nginx.tls.certresolver=myresolver'
      - "traefik.http.routers.nginx.middlewares=cors"
      - "traefik.http.middlewares.cors.headers.accesscontrolallowmethods=GET,OPTIONS,PUT"
      - "traefik.http.middlewares.cors.headers.accesscontrolalloworiginlist=*"
      - "traefik.http.middlewares.cors.headers.accesscontrolmaxage=100"
      - "traefik.http.middlewares.cors.headers.addvaryheader=true"
    depends_on: [consul]

  tanzania-ui:
    restart: always
    image: openlmistz/tanzania-ui:${TZ_UI_VERSION}
    env_file: settings.env
    depends_on: [consul]

  requisition:
    restart: always
    image: openlmis/requisition:${OL_REQUISITION_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx2048m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  referencedata:
    restart: always
    image: openlmis/referencedata:${OL_REFERENCEDATA_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx2048m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  auth:
    restart: always
    image: openlmis/auth:${OL_AUTH_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml -Dflyway.locations=classpath:db/migration,filesystem:/demo-data'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
      - 'auth-tomcat-log:/var/log/auth-tomcat'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  notification:
    restart: always
    image: openlmis/notification:${OL_NOTIFICATION_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Dspring.datasource.hikari.maximum-pool-size=5 -Xmx512m -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  fulfillment-extensions:
    image: openlmistz/tz-fulfillment-extensions:latest
    volumes:
      - 'fulfillment-extensions:/extensions'

  fulfillment:
    image: openlmis/fulfillment:${OL_FULFILLMENT_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dlogging.config=/config/log/logback.xml'
    volumes:
      - 'service-config:/config'
      - 'fulfillment-extensions:/extensions'
    depends_on: [log, db, fulfillment-extensions]
    command: ["/wait-for-postgres.sh", "/run.sh"]

 
  budget:
    restart: always
    image: openlmistz/budget:latest
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml -Dflyway.locations=classpath:db/migration,filesystem:/demo-data'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  tz-order-create-post-extension:
    restart: always
    image: openlmistz/tz-order-create-post-extension:${TZ_POST_EXTENSION_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]

  equipment:
    restart: always
    image: openlmistz/equipment:${OL_EQUIPMENT_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log, cce, auth, nginx]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  common:
    restart: always
    image: openlmistz/common:${TZ_COMMON_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: [ "/wait-for-postgres.sh", "/run.sh" ]
    
  cce:
    restart: always
    image: openlmis/cce:${OL_CCE_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  pcmt-integration:
    image: openlmistz/pcmt-integration:1.0.1-SNAPSHOT
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
    links:
      - db
      - log
    volumes:
      - 'service-config:/config'
    depends_on: [ log ]
    ports:
      - "8080"
    env_file: settings.env

  stockmanagement:
    restart: always
    image: openlmis/stockmanagement:${OL_STOCKMANAGEMENT_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml -Dflyway.locations=classpath:db/migration,filesystem:/demo-data'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  buq:
    restart: always
    image: openlmistz/buq:${OL_BUQ_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [ log ]
    command: [ "/wait-for-postgres.sh", "/run.sh" ]

  report:
    restart: always
    image: openlmis/report:${OL_REPORT_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml -Dflyway.locations=classpath:db/migration,filesystem:/demo-data'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]
    command: ["/wait-for-postgres.sh", "/run.sh"]

  hapifhir:
    restart: always
    image: openlmis/hapifhir:${OL_HAPIFHIR_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [ log ]
    command: [ "/wait-for-postgres.sh", "/run.sh" ]

  diagnostics:
    restart: always
    image: openlmis/diagnostics:${OL_DIAGNOSTICS_VERSION}
    env_file: settings.env
    environment:
      JAVA_OPTS: '-server -Xmx512m -Dspring.datasource.hikari.maximum-pool-size=5 -Dlogging.config=/config/log/logback.xml'
      spring_profiles_active: ${spring_profiles_active}
    volumes:
      - 'service-config:/config'
    depends_on: [log]

  db:
    restart: always
    image: openlmis/postgres:${OL_POSTGRES_VERSION}
    env_file: settings.env
    ports:
      - 5432:5432
    networks:
      default:
        aliases:
          - olmis-db
    volumes:
      - 'pg-data2:/var/lib/postgresql/data'
      - ./config/postgres/init.sql:/docker-entrypoint-initdb.d/tz-init.sql

    depends_on: [consul]

  log:
    restart: always
    image: openlmis/rsyslog:${OL_RSYSLOG_VERSION}
    volumes:
      - 'syslog:/var/log'
    depends_on:
      - service-configuration
      - consul

  scalyr:
    image: openlmis/scalyr
    container_name: scalyr-agent
    env_file: settings.env
    volumes:
      - 'service-config:/config'
      - '/run/docker.sock:/var/scalyr/docker.sock'
      - 'syslog:/var/log'
      - 'nginx-log:/var/log/nginx/log'
      - 'auth-tomcat-log:/var/log/auth-tomcat'

  service-configuration:
    build:
      context: ./config
    volumes:
      - service-config:/config

  ftp:
    image: hauptmedia/proftpd
    ports:
      - "21:21"
      - "20:20"
    env_file: settings.env
    depends_on: [consul]

  redis:
    image: redis:3.2.12
    depends_on: [consul]

volumes:
  syslog:
    external: false
  nginx-log:
    external: false
  auth-tomcat-log:
    external: false
  consul-template-log:
    external: false
  service-config:
    external: false
  pg-data2:
    external: true
  fulfillment-extensions:
    external: false   

networks:
  default:
    external: false
