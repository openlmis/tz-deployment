#!/usr/bin/env sh

# copy the credentials folder from gitlab variable
mkdir -p credentials

cat $ca_reporting > credentials/ca.pem
cat $cert_reporting > credentials/cert.pem
cat $key_reporting > credentials/key.pem
cat $settings_reporting > settings.env
