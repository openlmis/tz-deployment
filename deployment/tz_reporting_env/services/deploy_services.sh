#!/usr/bin/env bash

export DOCKER_TLS_VERIFY="1"
export COMPOSE_TLS_VERSION=TLSv1_2
export DOCKER_HOST="tcp://reports.3.elmis.co.tz:2376"
export DOCKER_CERT_PATH="${PWD}/credentials"
export REPORTING_DIR_NAME=reporting

git clone https://gitlab.com/openlmis/tz-distro.git ../distro

reportingRepo=$1

cp settings.env ../distro/reporting
cd ../distro/reporting

docker-compose kill
docker-compose down -v
docker rm $(docker ps -aq)
docker rmi $(docker images -aq)
docker volume rm $(docker volume ls -q)
docker-compose build --no-cache
docker-compose up --force-recreate -d
rm -r ../../distro