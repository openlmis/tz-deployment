#!/usr/bin/env sh

export KEEP_OR_WIPE="keep"

../shared/init_env_gh.sh

../shared/pull_images.sh $1

../shared/restart.sh $1
